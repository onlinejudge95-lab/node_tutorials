const { readFileSync, writeFileSync } = require("fs")

const firstFileHandler = readFileSync("./content/first.txt", "utf-8")
const secondFileHandler = readFileSync("./content/second.txt", "utf-8")

console.log(firstFileHandler)
console.log(secondFileHandler)

writeFileSync("./content/result-sync.txt", `New content:- ${firstFileHandler}${secondFileHandler}`, { flag: "a" })
