const { readFile, writeFile } = require("fs").promises

const start = async () => {
    try {
        const firstFileHandler = await readFile("./content/first.txt", "utf-8")
        const secondFileHandler = await readFile("./content/second.txt", "utf-8")

        await writeFile("./content/result-mind-grenade.txt", `New Content :- ${firstFileHandler} ${secondFileHandler}`, { flag: "a" })
    } catch (error) {
    }
}

start()
