var http = require("http")
var fs = require("fs")

http.createServer(function (request, response) {
    const fileStream = fs.createReadStream("./content/big.txt", "utf8")
    fileStream.on("open", () => fileStream.pipe(response))
    fileStream.on("error", error => response.end(error))
}).listen(5000)
