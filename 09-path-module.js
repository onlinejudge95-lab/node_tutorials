const path = require("path")
console.log(path.sep)

const filePath = path.join(".vscode", "settings.json")
console.log(filePath)
console.log(path.basename(filePath))

console.log(path.resolve(__dirname, filePath))
