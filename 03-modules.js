const names = require("./04-names")
const sayHello = require("./05-utils")
const data = require("./06-alternative-import")
require("./07-mind-grenade")

sayHello("test1")
sayHello(names.test2)
sayHello(names.test3)
console.log(data)
