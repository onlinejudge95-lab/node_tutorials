const http = require("http")

const server = http.createServer((req, res) => {
    if (req.url == "/") {
        res.end("Hello, world!")
    }else if (req.url == "/about") {
        res.end("Hello, world! about page")
    } else {
        res.end("Does not exists")
    }
})

server.listen(3000)
