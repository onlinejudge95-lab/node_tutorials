const http = require("http")

const server = http.createServer()

server.on("request", (request, response) => response.end("Welcome"))

server.listen(5000)
