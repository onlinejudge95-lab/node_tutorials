const EventEmitter = require("events")

const customEmitter = new EventEmitter()

customEmitter.on("response", (message, number) => console.log(`Message :- ${message} Number :- ${number}`))

customEmitter.on("response", () => console.log("Hello, world!"))

customEmitter.emit("response", "hello", 123)
